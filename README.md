CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Google Translate translator plugin for the Translation Management Tools (TMGMT)
project. Allows to use machine translation provided by Google to translate
content.

This modules uses the Google Translate Batch API to allow translating large
amount of content using Translation Management Tool (TMGMT) and Google
Translate.

 * Fast automated translation of content using Google Translate Service
 * Translate any entity, one or batch, by a few simple mouse clicks
 * Use advanced translation jobs management tool to submit and review
   translations

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/tmgmt_google_batch

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/tmgmt_google_batch


REQUIREMENTS
------------

This module requires the following module:

 * Translation Management Tool (http://drupal.org/project/tmgmt)

You need to have an account on Google Cloud Platform with an app that has
Google Translate API enabled.


INSTALLATION
------------

 * Install the TMGMT Translator Google module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420
   for further information.


CONFIGURATION
-------------

After installation, configure the module to work with your Google API key:

    1. After instllation, navigate to Administration > Translation >
       Providers > Google Batch API > Edit.
    2. Provide the API key for "Google Batch API Plugin Settings".
    3. Click "Save".


MAINTAINERS
-----------

Current maintainers:

 * Omar Alahmed - https://www.drupal.org/u/omar-alahmed
