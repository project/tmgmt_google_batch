<?php

namespace Drupal\tmgmt_google_batch\Plugin\tmgmt\Translator;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\TranslatorInterface;
use Google\Cloud\Translate\TranslateClient;
use Drupal\tmgmt\TranslatorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Google provider leveraging the Google API PHP library.
 *
 * @TranslatorPlugin(
 *   id = "google_batch_api",
 *   label = @Translation("Google Batch API"),
 *   description = @Translation("Leverage the Google batch API to perform translations."),
 *   ui = "Drupal\tmgmt_google_batch\GoogleTranslatorUi",
 *   logo = "icons/google.svg",
 * )
 */
class GoogleBatchTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Max number of text queries for translation sent in one request.
   *
   * @var int
   */
  protected $qChunkSize = 5;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedRemoteLanguages(TranslatorInterface $translator) {
    $languages = [];
    // Prevent access if the translator isn't configured yet.
    if (!$translator->getSetting('api_key')) {
      return $languages;
    }

    $client_config = [
      'key' => $translator->getSetting('api_key'),
    ];
    $translate = new TranslateClient($client_config);
    $languages = $translate->languages();

    return array_combine($languages, $languages);
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {
    $client_config = [
      'key' => $job->getTranslator()->getSetting('api_key'),
    ];
    $translate = new TranslateClient($client_config);
    $translation_configuration = [
      'source' => $job->getRemoteSourceLanguage(),
      'target' => $job->getRemoteTargetLanguage(),
    ];
    $job_items = $job->getItems();
    foreach ($job_items as $job_item) {
      // Pull the source data array through the job and flatten it.
      $data = \Drupal::service('tmgmt.data')
        ->filterTranslatable($job_item->getData());

      $translation = [];
      $q = [];
      $keys_sequence = [];
      $i = 0;

      // Build Google q param and preserve initial array keys.
      foreach ($data as $key => $value) {
        $q[] = $value['#text'];
        $keys_sequence[] = $key;
      }

      // Split $q into chunks of self::qChunkSize.
      foreach (array_chunk($q, $this->qChunkSize) as $_q) {

        // Get translation from Google.
        $translations = $translate->translateBatch($_q, $translation_configuration);

        // Collect translated texts with use of initial keys.
        foreach ($translations as $retrievd_translation) {
          $translation[$keys_sequence[$i]]['#text'] = $retrievd_translation['text'];
          $i++;
        }
      }

      // Save the translated data through the job.
      // NOTE that this line of code is reached only in case all translation
      // requests succeeded.
      $job_item->addTranslatedData(\Drupal::service('tmgmt.data')
        ->unflatten($translation));
    }
    if (!$job->isRejected()) {
      $job->submitted('The translation job has been submitted.');
    }
  }

}
